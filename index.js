console.log("Hello World");

// MY VARIABLES

let first = "First Name:", name = "Jane"; 
console.log(first,name);

let last =  "Last Name:", surname = "Doe";
console.log(last,surname);

let herAge = "Age" + ":" + 30;
console.log(herAge);

let hobbies = ["Reading", "Writing", "Watching TV"]
console.log("Hobbies:");
console.log(hobbies);

let myOffice = {
	houseNumber: 123,
	street: "Kayle St.",
	state: "California"
}
console.log("Work Address:");
console.log(myOffice);



// 2. DEBUGGING
	let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let age = 40;
	console.log("My current age is: " + age);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {
		userName: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,
	}
	console.log("My Full Profile:");
	console.log(profile);

	let bestFriend = "Bucky Barnes";
	console.log("My bestfriend is: " + bestFriend);

	const lastLocation = "Arctic Ocean";
	lastLocation[1] = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);